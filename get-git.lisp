;;;; get-git.lisp

(in-package #:get-git)

(defun git-conf-get (param)
  "Retrieve a git config parameter with an appropriate string, such as 'user.name'."
  (uiop:run-program (list "git" "config" param) :output :string))


(defun git-init (dir)
  "Run git init in the specified directory."
  (if (uiop:directory-exists-p dir)
      (uiop:run-program (list "git" "init" dir) :output :string)))




;; Outputs the correct config params to standard out!
(git-conf-get "user.name")
(git-conf-get "user.email")


(defparameter *git-user* (git-conf-get "user.name"))
(defparameter *git-email* (git-conf-get "user.email"))

(defun main ()
  (format t "Author: ~a~&Email: ~a~&" *git-user* *git-email*))
