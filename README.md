# get-git
### _Sam Johnson (she/her) sambjohnson94@gmail.com_

Utility lisp code to grab `git config` parameters as lisp variables. As the sidebar says, this is
part of an ongoing project to simplify Fukamachi's cl-project, by autopopulating the author and
email fields in the asdf specification of a new project. That itself is part of the greater effort 
to add `cargo new my_app`-type functionality to Common Lisp via an additional CLI flag to Roswell,
`new`. It's of this author's opinion that Common Lisp could benefit greatly from mimicing the CLI 
UX of both Rust's `cargo` and Javascript's `npm`, but that's for another time.

## License

MIT
