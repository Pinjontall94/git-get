;;;; package.lisp

(defpackage #:get-git
  (:use #:cl)
  (:export :main
           :git-conf-get))
